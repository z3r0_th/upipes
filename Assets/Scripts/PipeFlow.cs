﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeFlow : MonoBehaviour {

    private class PipeFlowing
    {
        public PipeBoardTile tile;
        public Vector2 currentPosition;
        public Vector2 targetPosition;
    }

    public PipeBoardTile tilePrefab;
    public RectTransform flowBoard;
    public int padding;
    public int flowBuffer = 10;
    public PipeFactory factory;
    public bool addNewTileWhenPop = true;

    private List<PipeFlowing> flowing = new List<PipeFlowing>();
    private int prefabHeight;
    private float timming;

    private void Start()
    {
        FillFlow();
    }

    private void PopTile()
    {
        if (flowing[0].targetPosition.y == 0)
        {
            Destroy(flowing[0].tile.gameObject);
            flowing.RemoveAt(0);
        }
    }

    private PipeFlowing AddPipeFlowing(int index, bool setup=false)
    {
        PipeBoardTile tile = GameObject.Instantiate(tilePrefab);
        tile.transform.SetParent(flowBoard, false);
        tile.Pipe = factory.GetRandomPipe();

        RectTransform tileRect = tile.transform as RectTransform;
        tileRect.pivot = new Vector2(0.5f, 0);
        tileRect.anchorMax = tileRect.anchorMin = new Vector2(0.5f, 0.0f);

        PipeFlowing flowingPipe = new PipeFlowing();
        flowingPipe.tile = tile;

        if (setup)
        {
            tileRect.anchoredPosition = new Vector2(0, (int)flowBoard.rect.height + index * prefabHeight);
            flowingPipe.currentPosition = tileRect.anchoredPosition;
            flowingPipe.targetPosition = new Vector2(0, index * prefabHeight);
        } else
        {
            tileRect.anchoredPosition = new Vector2(0, index * prefabHeight);
            tileRect.GetComponent<UnityEngine.UI.Image>().color = Color.red;
            flowingPipe.currentPosition = flowingPipe.targetPosition = tileRect.anchoredPosition;
        }

        return flowingPipe;
    }

    public void FillFlow()
    {
        prefabHeight = (int)(tilePrefab.transform as RectTransform).rect.height + padding;

        for (int i = flowing.Count; i < flowBuffer; ++i)
        {
            PipeFlowing flowingPipe = AddPipeFlowing(i, true);
            flowing.Add(flowingPipe);
        }
    }

    public void MoveFlow()
    {
        if (addNewTileWhenPop)
        {
            PipeFlowing flowingPipe = AddPipeFlowing(flowing.Count);
            flowing.Add(flowingPipe);
        }

        PopTile();

        for (int i = 0; i < flowing.Count; ++i)
        {
            flowing[i].currentPosition = (flowing[i].tile.transform as RectTransform).anchoredPosition;
            Vector2 pos = (flowing[i].tile.transform as RectTransform).anchoredPosition;
            pos.y = i * prefabHeight;
            flowing[i].targetPosition = pos;
        }

        timming = 0;
    }

    public void Update()
    {
        float timer = 0.25f;
        timming = Mathf.Min(timming + Time.deltaTime, timer);
        if (timming <= timer)
        {
            foreach (PipeFlowing pipeFlowing in flowing)
            {
                (pipeFlowing.tile.transform as RectTransform).anchoredPosition = Vector2.Lerp(pipeFlowing.currentPosition, pipeFlowing.targetPosition, timming / timer);
            }
        }
    }

    public Pipe CurrentPipe
    {
        get
        {
            return flowing[0].tile.Pipe;
        }
    }

    public bool HasPipe
    {
        get
        {
            return flowing.Count != 0;
        }
    }
}
