﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeFlowGameController : MonoBehaviour {

    public PipeBoard board;
    public PipeFlow flow;

	IEnumerator Start ()
    {
        int maxRows = board.MaxRows();
        int maxLines = board.MaxLines();

        board.GenerateEmptyBoard(maxRows, maxLines, new Vector2(0, 0), new Vector2(maxLines - 1, maxRows - 1));
        yield return new WaitForEndOfFrame();
        foreach(RectTransform tile in board.Board)
        {
            RectTransform _tile = tile;
            tile.GetComponentInChildren <UnityEngine.UI.Button>().onClick.AddListener(delegate
            {
                TileAction(_tile);
            });
        }
    }

    private void TileAction(RectTransform tiler)
    {
        PipeBoardTile tile = tiler.GetComponent<PipeBoardTile>();
        tile.Pipe = flow.CurrentPipe;
        flow.MoveFlow();
    }
}
