﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PipeType
{
    L = 0,
    X = 1,
    T = 2,
    I = 3
}

public enum PipeRotation
{
    R0 = 0,
    R90 = 1,
    R180 = 2,
    R270 = 3
}

public class Pipe : MonoBehaviour {

    [SerializeField]
    private PipeType pieceType;

    public PipeType PieceType
    {
        get
        {
            return pieceType;
        }
    }

    public PipeRotation Rotation
    {
        get
        {
            return CurrentPiece.Rotation;
        }
    }

    public PipePiece CurrentPiece
    {
        get
        {
            return pieces[currentPieceIndex];
        }
    }

    public PipePiece[] Pieces
    {
        get
        {
            return pieces;
        }
    }

    [SerializeField]
    private PipePiece [] pieces;
    private int currentPieceIndex = 0;

    private void Awake()
    {
        pieces = gameObject.GetComponentsInChildren<PipePiece>();

        System.Array.Sort(pieces, (x, y) =>
        {
            int c1 = (int)x.Rotation;
            int c2 = (int)y.Rotation;
            return c1.CompareTo(c2);
        });

        foreach(PipePiece pipe in pieces)
        {
            pipe.Pipe.enabled = false;
        }

        pieces[currentPieceIndex].Pipe.enabled = true;
    }

    private void OnValidate()
    {
        pieces = gameObject.GetComponentsInChildren<PipePiece>();
    }

    public void Rotate()
    {
        CurrentPiece.Pipe.enabled = false;
        currentPieceIndex = (currentPieceIndex + 1) % pieces.Length;
        CurrentPiece.Pipe.enabled = true;
    }
}
