﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Flags]
public enum Connection
{
    //None = 0x0,
    Top = 0x1,
    Botton = 0x2,
    Left = 0x4,
    Right = 0x8
}

public class PipePiece : MonoBehaviour {

    [SerializeField]
    private PipeRotation rotation;

    [EnumFlag]
    [SerializeField]
    private Connection connections;

    [SerializeField]
    private UnityEngine.UI.Image pipe;


    private void OnValidate()
    {
        pipe = GetComponent<UnityEngine.UI.Image>();
    }

    private void Awake()
    {
        if (pipe == null) pipe = GetComponent<UnityEngine.UI.Image>();
    }

    public PipeRotation Rotation
    {
        get
        {
            return rotation;
        }
    }


    public Connection Connections
    {
        get
        {
            return connections;
        }
    }

    public UnityEngine.UI.Image Pipe
    {
        get
        {
            return pipe;
        }
    }

}
