﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeFactory : MonoBehaviour {

    [SerializeField]
    private Pipe [] pipes;

	public Pipe GetPipe(PipeType type)
    {
        foreach(Pipe p in pipes)
        {
            if (p.PieceType == type)
            {
                return GameObject.Instantiate(p) as Pipe;
            }
        }

        return GameObject.Instantiate(pipes[0]) as Pipe;
    }

    private PipeType RandomType()
    {
        int random = Random.Range(0, 4);
        return (PipeType)(random);
    }

    public Pipe GetRandomPipe()
    {
        return GetPipe(RandomType());
    }

    public Pipe GetRandomPipesConnecting(Connection c1, Connection c2)
    {
        List<Pipe> possiblePipes = new List<Pipe>();
        foreach(Pipe p in pipes)
        {
            foreach(PipePiece piece in p.Pieces)
            {
                if ( (piece.Connections & c1) != 0 && (piece.Connections & c2) !=0 ) { possiblePipes.Add(p); continue; }
            }
        }

        if (possiblePipes.Count == 0)
        {
            Debug.LogError("No connection between [" + c1 + "] and [" + c2 + "]");
            return null;
        }

        return GameObject.Instantiate(possiblePipes[Random.Range(0, possiblePipes.Count)]);
    }
}
