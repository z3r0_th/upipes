﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeBoard : MonoBehaviour {

    private struct PipePath
    {
        public Pipe pipe;
        public Vector2 position;
    }

    #region Public Properties

    /// <summary>
    /// Rows in the board.
    /// </summary>
    public int Rows
    {
        get; private set;
    }

    /// <summary>
    /// Lines in the board
    /// </summary>
    public int Lines
    {
        get; private set;
    }

    public RectTransform Board
    {
        get
        {
            return board;
        }
    }

    #endregion

    #region Private fields

    [Tooltip("Single, empty, tile Prefab to be placed in board")]
    [SerializeField]
    private PipeBoardTile boardTilePrefab;

    [Tooltip("The board. Where all tile prefab will be placed")]
    [SerializeField]
    private RectTransform board;

    [Tooltip("Pipes prefab.")]
    [SerializeField]
    private PipeFactory factory;

    private Vector2 StartPoint;
    private Vector2 EndPoint;

    #endregion

    #region Private Methods

    /// <summary>
    /// Removes all tiles from this board
    /// </summary>
    private void RemoveTilesFromBoard()
    {
        foreach(Transform t in board.transform)
        {
            Destroy(t.gameObject);
        }
    }

    /// <summary>
    /// Rotates this tile so the piece's direction is a random one.
    /// </summary>
    private void RandomRotate(PipeBoardTile tile)
    {
        int rotation = Random.Range(0, 4);
        for (int i = 0; i < rotation; ++i)
        {
            tile.Rotate();
        }
    }

    /// <summary>
    /// Rotates all tiles in the board
    /// </summary>
    private void RandomRotateBoard()
    {
        Vector2 pos = Vector2.zero;
        for (int i = 0; i < Lines; ++i)
        {
            for (int j = 0; j < Rows; ++j)
            {
                pos.x = i;
                pos.y = j;
                RandomRotate(GetTile(pos));
            }
        }
    }

    /// <summary>
    /// Get a tile on specific position in the board
    /// Returns null if out of bouderies or not present
    /// </summary>
    private PipeBoardTile GetTile(Vector2 point)
    {
        if (point.x < 0 || point.x > Lines) return null;
        if (point.y < 0 || point.y > Rows) return null;

        int index = (int)(point.x * Rows + point.y);
        if (index >= board.childCount) return null;

        Transform t = board.GetChild(index);
        if (t == null) return null;
        return t.GetComponent<PipeBoardTile>();
    }

    /// <summary>
    /// Generate a Path of Pipes that goes from pointA to pointB.
    /// </summary>
    private List<PipePath> GeneratePath(Vector2 pointA, Vector2 pointB)
    {
        if (pointA.y > pointB.y)
        {
            Vector2 temp = pointB;
            pointB = pointA;
            pointA = temp;
        }

        List<PipePath> path = new List<PipePath>();

        Vector2 currentPoint = new Vector2(pointA.x, pointA.y);
        PipeBoardTile _tile = GetTile(currentPoint);
        _tile.GetComponent<UnityEngine.UI.Image>().color = Color.blue;

        Connection lastDirection = Connection.Right;
        Connection lastLastDirection = Connection.Right;

        PipeBoardTile lastTile = null;
        PipePath step = new PipePath();
        while (currentPoint.y != pointB.y || currentPoint.x != pointB.x)
        {
            lastLastDirection = lastDirection;
            if (currentPoint.y == pointB.y)
            {
                float d = Mathf.Sign(pointB.x - currentPoint.x);
                currentPoint.x += d;
                lastDirection = d > 0 ? Connection.Botton : Connection.Top;
            }
            else
            {
                List<Connection> possibleDir = new List<Connection>() { Connection.Right, Connection.Top, Connection.Botton };
                if (lastDirection == Connection.Botton || currentPoint.x == 0) possibleDir.RemoveAt(possibleDir.IndexOf(Connection.Top));
                if (lastDirection == Connection.Top || currentPoint.x == Lines - 1) possibleDir.RemoveAt(possibleDir.IndexOf(Connection.Botton));

                Connection dir = possibleDir[Random.Range(0, possibleDir.Count)];
                if (dir == Connection.Right)
                {
                    currentPoint.y++;
                }
                else if (dir == Connection.Top)
                {
                    currentPoint.x--;
                }
                else if (dir == Connection.Botton)
                {
                    currentPoint.x++;
                }

                lastDirection = dir;
            }

            if (lastTile != null)
            {
                step = new PipePath();
                step.pipe = factory.GetRandomPipesConnecting(lastDirection, oppositeFrom(lastLastDirection));
                step.position = new Vector2(lastTile.x, lastTile.y);
                path.Add(step);
                //lastTile.Pipe = factory.GetRandomPipesConnecting(lastDirection, oppositeFrom(lastLastDirection));
            }

            PipeBoardTile tile = GetTile(currentPoint);
            lastTile = tile;
        }
        return path;
    }

    Connection oppositeFrom(Connection dir)
    {
        if (dir == Connection.Right) return Connection.Left;
        if (dir == Connection.Top) return Connection.Botton;
        if (dir == Connection.Botton) return Connection.Top;
        return Connection.Left;
    }

    /// <summary>
    /// From this tile, returns all neighboors tile that has a connection.
    /// </summary>
    private PipeBoardTile[] GetConnectedNeighboors(PipeBoardTile tile)
    {
        List<PipeBoardTile> tiles = new List<PipeBoardTile>();
        if (tile == null || tile.Pipe == null) return tiles.ToArray();

        Connection con1 = tile.Pipe.CurrentPiece.Connections;
        PipeBoardTile nextTile = null;
        Connection con2 = Connection.Left;

        Vector2 p = new Vector2(tile.x, tile.y);

        nextTile = GetTile(new Vector2(p.x, p.y - 1));
        if (nextTile != null && nextTile.Pipe != null)
        {
            con2 = nextTile.Pipe.CurrentPiece.Connections;
            if (((con1 & Connection.Left) == Connection.Left) && ((con2 & Connection.Right) == Connection.Right))
            {
                tiles.Add(nextTile);
            }
        }


        nextTile = GetTile(new Vector2(p.x, p.y + 1));
        if (nextTile != null && nextTile.Pipe != null)
        {
            con2 = nextTile.Pipe.CurrentPiece.Connections;
            if (((con1 & Connection.Right) == Connection.Right) && ((con2 & Connection.Left) == Connection.Left))
            {
                tiles.Add(nextTile);
            }
        }

        nextTile = GetTile(new Vector2(p.x - 1, p.y));
        if (nextTile != null && nextTile.Pipe != null)
        {
            con2 = nextTile.Pipe.CurrentPiece.Connections;
            if (((con1 & Connection.Top) == Connection.Top) && ((con2 & Connection.Botton) == Connection.Botton))
            {
                tiles.Add(nextTile);
            }
        }

        nextTile = GetTile(new Vector2(p.x + 1, p.y));
        if (nextTile != null && nextTile.Pipe != null)
        {
            con2 = nextTile.Pipe.CurrentPiece.Connections;
            if (((con1 & Connection.Botton) == Connection.Botton) && ((con2 & Connection.Top) == Connection.Top))
            {
                tiles.Add(nextTile);
            }
        }


        return tiles.ToArray();
    }

    /// <summary>
    /// Internal recursive test if tile1 is connected to tile2
    /// </summary>
    private bool IsConnected(PipeBoardTile tile1, PipeBoardTile tile2, List<PipeBoardTile> visited)
    {
        if (tile1 == null || tile2 == null) return false;
        if (visited.Contains(tile1)) return false;
        visited.Add(tile1);

        if (tile1 == null || tile2 == null || tile1.Pipe == null || tile2.Pipe == null) return false;
        if (tile1 == tile2) return true;

        PipeBoardTile[] neighboors = GetConnectedNeighboors(tile1);

        foreach (PipeBoardTile b in neighboors)
        {
            if (IsConnected(b, tile2, visited)) return true;
        }

        return false;
    }

    /// <summary>
    /// Generates a new board. Populating all tiles
    /// </summary>
    private void GenerateNewBoard(int rows, int lines, Vector2 startingPoint, Vector2 endingPoint)
    {
        Rows = rows;
        Lines = lines;

        StartPoint = startingPoint;
        EndPoint = endingPoint;

        int tileHeight = (int)(boardTilePrefab.transform as RectTransform).rect.height;
        int tileWidth = (int)(boardTilePrefab.transform as RectTransform).rect.width;
        Vector2 anchor = new Vector2(0, 1);

        int boardHeight = (int)board.rect.height;
        int boardWidth = (int)board.rect.width;

        int totalTileHeightSize = tileHeight * lines;
        int totalTileWidthSize = tileWidth * rows;

        //Offset to adjust the tiles to free area; so all tiles are centered
        int offsetHeight = (boardHeight - totalTileHeightSize) / 2;
        int offsetWidth = (boardWidth - totalTileWidthSize) / 2;

        for (int i = 0; i < lines; ++i)
        {
            for (int j = 0; j < rows; ++j)
            {
                PipeBoardTile tile = GameObject.Instantiate(boardTilePrefab);

                tile.transform.SetParent(board.transform, false);
                tile.x = i;
                tile.y = j;
                tile.id = i * rows + j;
                tile.name = "Tile " + tile.id;

                RectTransform tileRect = tile.transform as RectTransform;
                tileRect.anchorMax = tileRect.anchorMin = anchor;
                tileRect.anchoredPosition = new Vector2(offsetWidth + j * tileWidth, -offsetHeight - i * tileHeight);
            }
        }

        GetTile(StartPoint).GetComponent<UnityEngine.UI.Image>().color = Color.red;
        GetTile(EndPoint).GetComponent<UnityEngine.UI.Image>().color = Color.green;
    }

    /// <summary>
    /// Put one random pipe in each tile in the board
    /// </summary>
    private void PopulatePipes()
    {
        foreach(Transform t in board.transform)
        {
            PipeBoardTile tile = t.GetComponent<PipeBoardTile>();
            tile.Pipe = factory.GetRandomPipe();
            tile.Pipe.transform.SetParent(tile.transform, false);
        }
    }

    /// <summary>
    /// Destroys all tiles, wait for end of frame (to garantee the removal from transform), and generate a new board
    /// </summary>
    private IEnumerator RemoveTilesAndGenerateNewBoardFilledWithPipes(int rows, int lines, Vector2 startingPoint, Vector2 endingPoint)
    {
        yield return RemoveTilesAndGenerateNewEmptyBoard(rows, lines, startingPoint, endingPoint);
        PopulateAllPipes();
    }

    private IEnumerator RemoveTilesAndGenerateNewEmptyBoard(int rows, int lines, Vector2 startingPoint, Vector2 endingPoint)
    {
        RemoveTilesFromBoard();
        yield return new WaitForEndOfFrame();
        GenerateNewBoard(rows, lines, startingPoint, endingPoint);
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Generate the board's tiles
    /// </summary>
    public void GenerateBoardFilledWithPipes(int rows, int lines, Vector2 startingPoint, Vector2 endingPoint)
    {
        StartCoroutine(RemoveTilesAndGenerateNewBoardFilledWithPipes(rows, lines, startingPoint, endingPoint));
    }

    public void GenerateEmptyBoard(int rows, int lines, Vector2 startingPoint, Vector2 endingPoint)
    {
        StartCoroutine(RemoveTilesAndGenerateNewEmptyBoard(rows, lines, startingPoint, endingPoint));
    }

    public void PopulateAllPipes()
    {
        PopulatePipes();
        List<PipePath> path = GeneratePath(StartPoint, EndPoint);
        foreach (PipePath p in path)
        {
            PipeBoardTile tile = GetTile(p.position);
            tile.GetComponent<UnityEngine.UI.Image>().color = Color.cyan;
            tile.Pipe = p.pipe;
        }

        RandomRotateBoard();
    }

    /// <summary>
    /// Test if there is any connection from StartPoint to EndPoint on the board
    /// </summary>
    public bool IsConnected()
    {
        List<PipeBoardTile> visited = new List<PipeBoardTile>();
        return IsConnected(GetTile(StartPoint), GetTile(EndPoint), visited);
    }

    public int MaxRows()
    {
        int boardWidth = (int)(board.transform as RectTransform).rect.width;
        int tileWidth = (int)(boardTilePrefab.transform as RectTransform).rect.width;
        return (int)Mathf.Floor(boardWidth / tileWidth);
    }

    public int MaxLines()
    {
        int boardHeight = (int)(board.transform as RectTransform).rect.height;
        int tileHeight = (int)(boardTilePrefab.transform as RectTransform).rect.height;
        return (int)Mathf.Floor(boardHeight / tileHeight);
    }

    #endregion
}
