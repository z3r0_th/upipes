﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugControl : MonoBehaviour {

    public int boardRows;
    public int boardLines;
    public bool autoAdjust = true;

    public PipeBoard board;

    private void OnGUI()
    {
        if (GUILayout.Button("Generate Board With Pipes"))
        {
            if (autoAdjust)
            {
                int maxRows = board.MaxRows();
                int maxLines = board.MaxLines();
                boardRows = maxRows;
                boardLines = maxLines;
            }

            board.GenerateBoardFilledWithPipes(boardRows, boardLines, new Vector2(0,0), new Vector2(boardLines - 1, boardRows - 1));
        }

        if (GUILayout.Button("Generate Empty Board"))
        {
            if (autoAdjust)
            {
                int maxRows = board.MaxRows();
                int maxLines = board.MaxLines();
                boardRows = maxRows;
                boardLines = maxLines;
            }

            board.GenerateEmptyBoard(boardRows, boardLines, new Vector2(0, 0), new Vector2(boardLines - 1, boardRows - 1));
        }

        if (GUILayout.Button("Test Connection"))
        {
            Debug.Log("<b>" + board.IsConnected() + "</b>");
        }
    }
}
