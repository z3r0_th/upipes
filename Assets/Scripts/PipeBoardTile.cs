﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeBoardTile : MonoBehaviour {

    public int x;
    public int y;
    public int id;

    public Pipe Pipe
    {
        get
        {
            return pipe;
        }

        set
        {
            if (pipe != null)
                Destroy(pipe.gameObject);
            pipe = value;
            if (pipe != null)
                pipe.transform.SetParent(this.transform, false);
        }
    }


    [SerializeField]
    private Pipe pipe;
    
    public void Rotate()
    {
        if (pipe != null)
            pipe.Rotate();
    }


}
